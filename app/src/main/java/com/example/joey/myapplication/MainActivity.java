package com.example.joey.myapplication;

import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import java.time.LocalDateTime;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity implements SensorEventListener {

    SensorManager sensorManager;
    Sensor mAcceleromter;

    protected void registerAccelerometer(){
        Thread thread = new Thread(){
            public void run(){
                try{
                    Thread.sleep(500);
                    sensorManager.registerListener(MainActivity.this, mAcceleromter, SensorManager.SENSOR_DELAY_GAME);
                } catch (InterruptedException e){

                }
            }
        };
        thread.run();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        mAcceleromter = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerAccelerometer();
    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() != Sensor.TYPE_ACCELEROMETER)
            return;

        TextView tv = findViewById(R.id.textView3);

        double sumOfForces = Math.sqrt(Math.pow(event.values[0], 2) + Math.pow(event.values[1], 2) + Math.pow(event.values[2], 2));
        tv.setText( Double.toString(sumOfForces));

        if(sumOfForces < 0.2 * 9.8){
            sensorManager.unregisterListener(this);
            Intent intent = new Intent(this, Throwing.class);
            intent.putExtra("StartTime", System.currentTimeMillis());
            startActivityForResult(intent, 1);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 1 && resultCode == RESULT_OK){
            double totalTime = (data.getLongExtra("EndTime", 0) - data.getLongExtra("StartTime", 0)) / 1000.0;
            TextView tv = findViewById(R.id.textView4);
            tv.setText(Double.toString(totalTime) + " throw time.");
            registerAccelerometer();
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
